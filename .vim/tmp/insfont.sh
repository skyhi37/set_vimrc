#!/bin/bash
yum install ttmkfdir.x86_64 xorg-x11-font-utils.x86_64
 
mkdir /usr/share/fonts/default/TrueType
cp *.otf /usr/share/fonts/default/TrueType
cd /usr/share/fonts/default/TrueType
ttmkfdir
mkfontdir
fc-cache
 
# view installed fonts:
/usr/bin/fc-list
